package org.apache.lucene.queryparser.flexible.standard.processors;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.AnalyzerWrapper;
import org.apache.lucene.analysis.MockAnalyzer;
import org.apache.lucene.analysis.MockTokenFilter;
import org.apache.lucene.analysis.MockTokenizer;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.flexible.core.config.QueryConfigHandler;
import org.apache.lucene.queryparser.flexible.core.nodes.FuzzyQueryNode;
import org.apache.lucene.queryparser.flexible.core.nodes.QueryNode;
import org.apache.lucene.queryparser.flexible.standard.config.StandardQueryConfigHandler;
import org.apache.lucene.queryparser.flexible.standard.config.StandardQueryConfigHandler.ConfigurationKeys;
import org.apache.lucene.queryparser.flexible.standard.nodes.PrefixWildcardQueryNode;
import org.apache.lucene.queryparser.flexible.standard.nodes.WildcardQueryNode;
import org.apache.lucene.util.LuceneTestCase;

/**
 * Adapted from TestAnalyzingQueryParser
 */
public class TestAnalyzerQueryNodeProcessor extends LuceneTestCase {

  private final static String FIELD = "field";

  private AnalyzerQueryNodeProcessor getProcessor() {
    Analyzer analyzer = new NormalizingMockAnalyzer(new MockAnalyzer(random(), MockTokenizer.WHITESPACE, true, MockTokenFilter.ENGLISH_STOPSET));
    QueryConfigHandler configHandler = new StandardQueryConfigHandler();
    configHandler.set(ConfigurationKeys.ANALYZER, analyzer);
    AnalyzerQueryNodeProcessor processor = new AnalyzerQueryNodeProcessor();
    processor.setQueryConfigHandler(configHandler);
    return processor;
  }

  public void testPrefixNothingToBeSubstituted() throws Exception {
    String termStr = "the*";
    PrefixWildcardQueryNode inputNode = new PrefixWildcardQueryNode(FIELD, termStr, 0, termStr.length());

    AnalyzerQueryNodeProcessor processor = getProcessor();
    QueryNode normalized = processor.process(inputNode);

    verifyPrefixWildcardQueryNode(inputNode, normalized);
  }

  public void testPrefixUmlautSubstituted() throws Exception {
    String termStr = "Mötl*";
    PrefixWildcardQueryNode inputNode = new PrefixWildcardQueryNode(FIELD, termStr, 0, termStr.length());

    AnalyzerQueryNodeProcessor processor = getProcessor();
    QueryNode normalized = processor.process(inputNode);

    verifyPrefixWildcardQueryNode(new PrefixWildcardQueryNode(FIELD, "motl*", 0, termStr.length()), normalized);
  }

  public void testPrefixNotHandlingMultipleTokens() throws Exception {
    String termStr = "Mötley Cr*";
    PrefixWildcardQueryNode inputNode = new PrefixWildcardQueryNode(FIELD, termStr, 0, termStr.length());

    AnalyzerQueryNodeProcessor processor = getProcessor();
    QueryNode normalized = processor.process(inputNode);

    verifyPrefixWildcardQueryNode(inputNode, normalized);
  }

  public void testWildcardNothingToBeSubstituted() throws Exception {
    String termStr = "re*ion";
    WildcardQueryNode inputNode = new WildcardQueryNode(FIELD, termStr, 0, termStr.length());

    AnalyzerQueryNodeProcessor processor = getProcessor();
    QueryNode normalized = processor.process(inputNode);

    verifyWildcardQueryNode(inputNode, normalized);
  }

  public void testWildcardUmlautSubstituted() throws Exception {
    String termStr = "Mötl?y";
    WildcardQueryNode inputNode = new WildcardQueryNode(FIELD, termStr, 0, termStr.length());

    AnalyzerQueryNodeProcessor processor = getProcessor();
    QueryNode normalized = processor.process(inputNode);

    verifyWildcardQueryNode(new WildcardQueryNode(FIELD, "motl?y", 0, termStr.length()), normalized);
  }

  public void testWildcardMultipleChunks() throws Exception {
    String termStr = "Möb?lträg*füße";
    WildcardQueryNode inputNode = new WildcardQueryNode(FIELD, termStr, 0, termStr.length());

    AnalyzerQueryNodeProcessor processor = getProcessor();
    QueryNode normalized = processor.process(inputNode);

    verifyWildcardQueryNode(new WildcardQueryNode(FIELD, "mob?ltrag*fusse", 0, termStr.length()), normalized);
  }

  public void testWildcardNotHandlingMultipleTokens() throws Exception {
    String termStr = "Möb?lträg*füß e";
    WildcardQueryNode inputNode = new WildcardQueryNode(FIELD, termStr, 0, termStr.length());

    AnalyzerQueryNodeProcessor processor = getProcessor();
    QueryNode normalized = processor.process(inputNode);

    verifyWildcardQueryNode(inputNode, normalized);
  }

  public void testFuzzyNothingToBeSubstituted() throws Exception {
    String termStr = "the";
    FuzzyQueryNode inputNode = new FuzzyQueryNode(FIELD, termStr, 0.5f, 0, termStr.length());

    AnalyzerQueryNodeProcessor processor = getProcessor();
    QueryNode normalized = processor.process(inputNode);

    verifyFuzzyQueryNode(inputNode, normalized);
  }

  public void testFuzzyUmlautSubstituted() throws Exception {
    String termStr = "Möbelträgerfüße";
    FuzzyQueryNode inputNode = new FuzzyQueryNode(FIELD, termStr, 0.5f, 0, termStr.length());

    AnalyzerQueryNodeProcessor processor = getProcessor();
    QueryNode normalized = processor.process(inputNode);

    verifyFuzzyQueryNode(new FuzzyQueryNode(FIELD, "mobeltragerfusse", 0.5f, 0, termStr.length()), normalized);
  }

  public void testFuzzyNotHandlingMultipleTokens() throws Exception {
    String termStr = "Mötley Crüe";
    FuzzyQueryNode inputNode = new FuzzyQueryNode(FIELD, termStr, 0.5f, 0, termStr.length());

    AnalyzerQueryNodeProcessor processor = getProcessor();
    QueryNode normalized = processor.process(inputNode);

    verifyFuzzyQueryNode(inputNode, normalized);
  }

  private void verifyPrefixWildcardQueryNode(PrefixWildcardQueryNode expected, QueryNode actual) {
    assertTrue(actual instanceof PrefixWildcardQueryNode);
    verifyWildcardQueryNode(expected, actual);
  }

  private void verifyWildcardQueryNode(WildcardQueryNode expected, QueryNode actual) {
    assertTrue(actual instanceof WildcardQueryNode);
    WildcardQueryNode normalizedWildcardQN = (WildcardQueryNode) actual;
    assertEquals(expected.getTextAsString(), normalizedWildcardQN.getTextAsString());
    assertEquals(expected.getFieldAsString(), normalizedWildcardQN.getFieldAsString());
    assertEquals(expected.getBegin(), normalizedWildcardQN.getBegin());
    assertEquals(expected.getEnd(), normalizedWildcardQN.getEnd());
  }

  private void verifyFuzzyQueryNode(FuzzyQueryNode expected, QueryNode actual) {
    assertTrue(actual instanceof FuzzyQueryNode);
    FuzzyQueryNode normalizedFuzzyQN = (FuzzyQueryNode) actual;
    assertEquals(expected.getTextAsString(), normalizedFuzzyQN.getTextAsString());
    assertEquals(expected.getFieldAsString(), normalizedFuzzyQN.getFieldAsString());
    assertEquals(expected.getSimilarity(), normalizedFuzzyQN.getSimilarity(), 0.001);
    assertEquals(expected.getBegin(), normalizedFuzzyQN.getBegin());
    assertEquals(expected.getEnd(), normalizedFuzzyQN.getEnd());
  }

  final static class NormalizingFilter extends TokenFilter {
    final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);

    public NormalizingFilter(TokenStream input) {
      super(input);
    }

    @Override
    public boolean incrementToken() throws IOException {
      if (input.incrementToken()) {
        char term[] = termAtt.buffer();
        int length = termAtt.length();
        for (int i = 0; i < length; i++) {
          switch (term[i]) {
            case 'ä':
              term[i] = 'a';
              break;
            case 'ë':
              term[i] = 'e';
              break;
            case 'ï':
              term[i] = 'i';
              break;
            case 'ö':
              term[i] = 'o';
              break;
            case 'ü':
              term[i] = 'u';
              break;
            case 'ß':
              term[i++] = 's';
              term = termAtt.resizeBuffer(length + 1);
              if (i < length) {
                System.arraycopy(term, i, term, i + 1, (length - i));
              }
              term[i] = 's';
              length++;
              break;
            case 'é':
              term[i] = 'e';
              break;
          }
        }
        termAtt.setLength(length);
        return true;
      } else {
        return false;
      }
    }
  }

  final static class NormalizingMockAnalyzer extends AnalyzerWrapper {

    private final MockAnalyzer mockAnalyzer;

    public NormalizingMockAnalyzer(MockAnalyzer mockAnalyzer) {
      super();
      this.mockAnalyzer = mockAnalyzer;
    }

    @Override
    protected Analyzer getWrappedAnalyzer(String fieldName) {
      return this.mockAnalyzer;
    }

    @Override
    protected TokenStreamComponents wrapComponents(String fieldName, TokenStreamComponents components) {
      return new TokenStreamComponents(components.getTokenizer(), components.getTokenStream(), new NormalizingFilter(components.getTokenStream()));
    }
  }
}
