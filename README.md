**Apache Lucene/Solr**

lucene/ is a search engine library
solr/ is a search engine server that uses lucene

This repository is forked from [Apache Lucene/Solr library v4.4.0](http://svn.apache.org/repos/asf/lucene/dev/tags/lucene_solr_4_4_0) and that contains *only* the fixed **UAX29URLEmailTokenizer** (backported from 4.9.1)
See more detail in [https://issues.apache.org/jira/browse/LUCENE-5400](https://issues.apache.org/jira/browse/LUCENE-5400)

* To compile the sources run: **ant compile**
* To run all the tests run: **ant test**

**How to release a specific version for Lucene:**
See this [Build With Maven](https://bitbucket.org/atlassian/atlassian-lucene/src/f1220ba40984cc7a2f36794d20dc36501c2f8d03/dev-tools/maven/README.maven?at=master)

Here are specific steps:

```
cd atlassian-lucene
export ANT_OPTS=-Xmx1g
ant -Dversion=4.4.0-atlassian-01 generate-maven-artifacts
```

Next step, we will deploy all of our generated jar and source files to our [Atlassian 3rdparty Repository](https://maven.atlassian.com/3rdparty) by using below command for each of files:

```
mvn deploy:deploy-file -Durl=https://maven.atlassian.com/3rdparty -DrepositoryId=atlassian-3rdparty -Dfile=org/apache/lucene/lucene-core/4.4.0-atlassian-01/lucene-core-4.4.0-atlassian-01.jar -DgroupId=org.apache.lucene -DartifactId=lucene-core -Dversion=4.4.0-atlassian-01 -Dpackaging=jar -DgeneratePom=true -Dsources=org/apache/lucene/lucene-core/4.4.0-atlassian-01/lucene-core-4.4.0-atlassian-01-sources.jar
```

In order to release this easier, i wrote a small script to deploy all lucene libraries to atlassian 3rd party repository:
[deploy-atlassian-3rdparty.sh](https://bitbucket.org/atlassian/atlassian-lucene/src/562b127f3afbcb47f8f31efb290078de53ecd745/deploy-atlassian-3rdparty.sh?at=master)

```
chmod 755 deploy-atlassian-3rdparty.sh
./deploy-atlassian-3rdparty.sh <LUCENE_RELEASE_VERSION>
```

For e.g.: **./deploy-atlassian-3rdparty.sh 4.4.0-atlassian-01**